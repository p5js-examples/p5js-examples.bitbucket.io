// Bouncing ball

let v;
let y;
let x;
const gravity= 0.98;
const size = 50;
const minVelocity= 1;
const damping=0.7;

function setup (){
  createCanvas(400, 300);
  v=0;
  y= height-size/2;
  x= 0;
}

function draw(){
  background(120)
  stroke(0);
  fill(255);

  y+= v;
  v+= gravity;

  if (y>height-size/2)
  {
    y= height-size/2;
    v= 1 + v * - damping; // add a fix cost of 1 when bouncing
    if (abs(v)<minVelocity)v=0; // stop the ball
  }

  ellipse(x, y, size, size);
}

function mousePressed()
{
  v=0;
  y= mouseY;
  x= mouseX;
}