// Ball example using p5.Vector

let v;
let pos;
let gravity;
const size = 50;
const minVelocity= 10;
const damping=0.7;

function setup (){
  createCanvas(400, 300);
  v= createVector(2, 0);
  pos= createVector(size/2,height-size/2)
  gravity= createVector(0, 0.98);
}

function draw(){
  background(120)
  stroke(0);
  fill(255);

  pos.add(v)
  v.add(gravity)
  
  // y
  if (pos.y>height-size/2)
  {
    pos.y= height-size/2;
    v.y= 1 + v.y * - damping; // add a fix cost of 1 when bouncing
    
    if (abs(v.y)<1)v.y=0; // stop the ball
  }

  v.x *= .98;

  if (pos.x <= size/2 || pos.x>= width-size/2)
  {
    v.x= -v.x;
  }

  ellipse(pos.x, pos.y, size, size);
}

function mousePressed()
{
  v= createVector(10, 0);
  pos= createVector(mouseX, mouseY)
}