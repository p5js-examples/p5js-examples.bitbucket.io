// P5.GUI example :Adapted from Slider-Range Example 4                                //
// See here: https://p5js.org/examples/instance-mode-instantiation.html

let params = {
  // seeds
  seeds: 500,
  seedsMin: 1,
  seedsMax: 2000,

  // angle (phi)
  angle: (360 * (Math.sqrt(5) - 1)) / 2,
  angleMax: 360,
  angleStep: 0.1,

  // radius of the seed
  radius: 3,
  radiusMin: 0.5,
  radiusMax: 5,
  radiusStep: 0.1,

  seedColor: "#ffffff",

  // scale
  zoom: 5,
  zoomMax: 50,
  zoomStep: 0.1,

  opacity: 150,
  opacityMax: 255,

  bgColor: [5, 255, 255],
};

// the gui object
let gui;

// the container of the canvas
let div;

function setup() 
{
  createCanvas(400, 300);
  // or ... create a canvas that fills the div
  // div = canvas.parentElement;
  // createCanvas(div.clientWidth, div.clientHeight);

  // color mode used for interpreting param colors
  colorMode(HSB, 100, 255, 255, 255);

  // all angles in degrees (0 .. 360)
  angleMode(DEGREES);

  // create the GUI from a settings object
  gui = createGui(this);
  gui.addObject(params);

  // only call draw when then gui is changed
  noLoop();
}

function draw() 
{
  // hello darkness my old friend
  background(params.bgColor);

  // let the seeds be filleth
  let c = color(params.seedColor);
  fill(hue(c), saturation(c), brightness(c), params.opacity);
  stroke(0, params.opacity);

  // absolute radius
  let r = params.radius * params.zoom;

  push();

  // go to the center of the sunflower
  translate(width / 2, height / 2);

  // rotate around the center while going outwards
  for (let i = 0; i < params.seeds; i++) {
    push();
    rotate(i * params.angle);
    // distance to the center of the sunflower
    let d = sqrt(i + 0.5) * params.zoom;
    ellipse(d, 0, r, r);
    pop();
  }

  pop();
}
