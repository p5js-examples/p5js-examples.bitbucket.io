// Drawing pixelite camera

let img;
const size = 8;

function setup (){
  createCanvas(400, 300);
  // img is the camera
  img = createCapture(VIDEO);
  img.size(400, 300);
  img.hide();

  ellipseMode(CORNER);
  noStroke();
}


function draw(){
  background(255)
  
  img.loadPixels();

  for (let x = 0; x<img.width; x+=size)
    for (let y=0; y<img.height; y+=size)
    {
      let c= getAverageColor (img, x, y, size);
      fill(0);
      // fill(c);
      // rect(x,y,size, size);
      let szCircle = map(c, 255, 120, 0, size); // 120 chosen empirically
      ellipse(x,y,szCircle, szCircle);
    }
}

function getAverageColor (img, xStart, yStart, size)
{
  let result =0; // black
  for (let x = 0; x<size; x++)
    for (let y= 0; y<size; y++)
    {
      let arr= getPixel (img, xStart+x, yStart+y);
      let colPixel= (arr[0]+arr[1]+arr[2])/3;
      result += colPixel;
    }
  return result/(size*size);
}



function getPixel(img, x, y)
{
  let i = 4 * (y * img.width + x);
  let r = img.pixels[i];
  let g = img.pixels[i+1];
  let b = img.pixels[i+2];
  let a = img.pixels[i+3];

  return [r,g,b,a];
}