// Analog watch js


// craeting two objects
const watch1 = {};
const watch2 = {};


function setup() 
{
	createCanvas(400, 300);

	initWatch (watch1, width/3, height/2, 50, 10);
	initWatch (watch2, 2*width/3, height/2, 50, 100);

	// you can change the color of the second watch also directly
	watch2.bgColor= color (204, 155, 200);
}

function draw()
{
	background(200);
	drawWatch (watch1);
	drawWatch (watch2);
}

// function serves as constructor
function initWatch (w, x, y, size, refreshMs)
{
	w.loc= createVector (x, y);
	w.size= size;
	w.bgColor= color(255, 204, 0);
	w.hour = 0;
	w.min = 0;
	w.sec = 0;

	setInterval ( () => {
		w.sec++;
		if (w.sec>=60) { w.min++; w.sec=0; }
		if (w.min>=60) { w.hour++; w.min=0; }
		if (w.hour>=24) w.hour=0;
	}
	, refreshMs);
}

// drawing function
function drawWatch(w)
{
	ellipseMode(CENTER);
	stroke(0);
	strokeWeight(w.size/10);
	fill(w.bgColor);
	ellipse(w.loc.x, w.loc.y, w.size*2, w.size*2);
	// drawLineWithAngle (w.loc.x, w.loc.y, 0, w.size);
	drawSec (w.loc.x, w.loc.y, w.sec, w.size)
	drawMin (w.loc.x, w.loc.y, w.min, w.size)
	drawHour (w.loc.x, w.loc.y, w.hour, w.size)

	// inner functions (hidden)
	function drawSec (x, y, sec, length)
	{
		const a= -PI/2 + 2*PI*sec/60.0;
		strokeWeight (length/50);
		stroke(255,0,0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawMin (x, y, min, length)
	{
		const a= -PI/2 + 2*PI*min/60.0;
		strokeWeight (length/30);
		stroke(0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawHour (x, y, hour, length)
	{
		if (hour>12) hour-=12;
		const a= -PI/2 + 2*PI*hour/12.0;
		strokeWeight (length/20);
		stroke(0);
		drawLineWithAngle(x, y, a, length*4.0/5);
	}

	function drawLineWithAngle (x, y, a, length)
	{
		translate(x, y);
		rotate(a);
		line(0,0,length,0);
		resetMatrix();
	}
}